import React from "react"
import { Layout, Row, Col } from "antd"
import Logo from "./logo"

const { Header } = Layout

export default ()=>{

  return (
    <Header className="header">
      <Row>
        <Col span={12}>
          <Logo />
        </Col>
        <Col span={12} style={{textAlign:'right'}}>
        </Col>
      </Row>
    </Header>
  )  
}