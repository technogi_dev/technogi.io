import React, { useState } from "react"
import { Link } from "gatsby"
import { Layout, Menu, Icon } from "antd"
import SEO from "./seo"
import Header from "./header"
import "./layout.less"

const { Content, Sider } = Layout

const AppLayout = ({ children, location = { pathname: "" }, title, seo }) => {
  const [collapsed, setCollapsed] = useState(false)
  return (
    <>
      <SEO title={seo || title || "TECHNOGI"} />
      <Layout>
        {/*<Header />*/}
        <Layout>
          <Sider
            collapsed={collapsed}
            onCollapse={setCollapsed}
            width={200}
            className="app-side-bar"
          >
            <Menu
              mode="inline"
              defaultSelectedKeys={[location.pathname]}
              defaultOpenKeys={[]}
              className="app-side-bar-menu"
            >
              <Menu.Item key="/">
                <Link to="/">
                  <Icon type="home" />
                  <span>Inicio</span>
                </Link>
              </Menu.Item>

              <Menu.Item key="/equipment">
                <Link to="/equipment">
                  <Icon type="laptop" />
                  <span>Equipo</span>
                </Link>
              </Menu.Item>

              {/*<SubMenu
              key="sub1"
              title={
                <span>
                  <Icon type="user" />
                  <span>Usuarios</span>
                </span>
              }
            >
              <Menu.Item key="1">option1</Menu.Item>
              <Menu.Item key="2">option2</Menu.Item>
              <Menu.Item key="3">option3</Menu.Item>
            <Menu.Item key="4">option4</Menu.Item>
            </SubMenu>*/}
            </Menu>
          </Sider>
          <Layout className="app-container">
            {/*
              <Breadcrumb style={{ margin: "16px 0" }}>
              <Breadcrumb.Item>Home</Breadcrumb.Item>
              <Breadcrumb.Item>List</Breadcrumb.Item>
              <Breadcrumb.Item>App</Breadcrumb.Item>
            </Breadcrumb>
            */}
            <h1>{title}</h1>
            <Content className="app-content">{children}</Content>
          </Layout>
        </Layout>
      </Layout>
    </>
  )
}

export default AppLayout
