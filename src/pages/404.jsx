import React from "react"
import { Result, Button } from "antd"

import Layout from "../components/layout"

const NotFoundPage = () => (
  <Layout title="404: Not found">
    <Result
      status="404"
      title="404"
      subTitle="Sorry, the page you visited does not exist."
      extra={<Button type="primary">Regresar</Button>}
    />
  </Layout>
)

export default NotFoundPage
