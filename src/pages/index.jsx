import React from "react"
import { Result } from "antd"

import Layout from "../components/layout"
import HomeImg from "../images/home-image"

const IndexPage = ({ location }) => (
  <Layout title="" seo="Home" location={location}>
    <Result
      icon={<HomeImg />}
      title="Bienvenido a TECHNOGI.io"
      subTitle="Sistema Administrativo de Technogi"
    />
  </Layout>
)

export default IndexPage
